<?php
    $users = []; 
    $users["5"] = ["name" => "Test", "email" => "test@test.com", "lang" => "en"];
    $users["3"] = ["name" => "Anton", "email" => "anton@gmail.com", "lang" => "ua"];
    $users["4"] = ["name" => "Anna", "email" => "anna@test.com", "lang" => "ru"];
    $users["2"] = ["name" => "Mike", "email" => "mike@gmail.com", "lang" => "fr"];
    $users["1"] = ["name" => "Jon", "email" => "jon@test.com", "lang" => "de"];
    $users["7"] = ["name" => "Jane", "email" => "jane@gmail.com", "lang" => "en"];

    echo "Number of users : ".count($users)."<br/>";

    echo "<pre>";
    krsort($users);
    print_r($users);
    echo "</pre>";

    echo "Users with max ID = ".reset($users)['name']."<br/>";
    echo "Users with max ID-1= ".next($users)['name']."<br/>";
    echo "Users with min ID = ".end($users)['name']."<br/>";
    echo "Users with min ID+1= ".prev($users)['name']."<br/>";
   

    $minID = min(array_keys($users));
    unset($users[ $minID]);
    echo "<pre>";
    print_r($users);
    echo "</pre>";
?>    