<?php 
    function UserDataToArray($fileName)
    {
        if(file_exists($fileName)){
            $contents = file_get_contents($fileName);
            $contents = utf8_encode($contents);
            $userData = json_decode($contents, true);
            return $userData;
        }else die("File ".$fileName." not found");
    }
    
    /*echo "<pre>";
    print_r(UserDataToArray("UsersData.json"));
    echo "</pre>";*/

    function IdentificationUser($login, $password, $arrayUsers)
    {
        if (!empty($login) && !empty($password) && !empty($arrayUsers)){
            $equal = FALSE;
            foreach ($arrayUsers as $user){
                if (($user['login'] == $login) && ($user['password'] == $password)){
                    $equal = TRUE;
                    return $user;
                }
            }
            if ($equal == FALSE){
                die("User not found");
            }
        }
    }

    echo "<pre>";
    print_r(IdentificationUser("loginTest", "1234568888", UserDataToArray("UsersData.json")));
    echo "</pre>";
?>