<?php
    $fileName = "UsersData.json";
    if(file_exists($fileName)){
        $contents = file_get_contents($fileName);
        $contents = utf8_encode($contents);
        $userData = json_decode($contents, true);
        return $userData;
    }else die("File ".$fileName." not found");
?>