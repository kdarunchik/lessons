<?php
    $users = []; 
    $users["5"] = ["name" => "Test", "email" => "test@test.com", "lang" => "de"];
    $users["3"] = ["name" => "Anton", "email" => "anton@gmail.com", "lang" => "ua"];
    $users["4"] = ["name" => "Anna", "email" => "anna@test.com", "lang" => "ru"];
    $users["2"] = ["name" => "Mike", "email" => "mike@gmail.com", "lang" => "fr"];
    $users["1"] = ["name" => "Jon", "email" => "jon@test.com", "lang" => "de"];
    $users["7"] = ["name" => "Jane", "email" => "jane@gmail.com", "lang" => "ua"];

    $language = ["en" => "Hello!",
                "ua" => "Доброго дня!",
                "ru" => "Привет!",
                "fr" => "Bonjour!",
                "de" => "Guten Tag!"];

    if ((!empty(reset($users)['lang'])) && (!empty(end($users)['lang']))){
        if (reset($users)['lang'] == end($users)['lang']){
            echo $language[reset($users)['lang']];
        }else{
            echo $language[reset($users)['lang']]."<br/>";
            echo $language[end($users)['lang']]."<br/>";
        }
    }
    
?>    