<?php
    // База клієнтів
    $users = []; 
    $users["5"] = ["name" => "Anna", "email" => "test@test.com", "lang" => "en"];
    $users["3"] = ["name" => "Anton", "email" => "anton@gmail.com", "lang" => "ua"];
    $users["4"] = ["name" => "Anna", "email" => "anna@test.com", "lang" => "ru"];
    $users["2"] = ["name" => "Mike", "email" => "mike@gmail.com", "lang" => "fr"];
    $users["1"] = ["name" => "Jane", "email" => "jon@test.com", "lang" => "de"];
    $users["7"] = ["name" => "Jane", "email" => "jane@gmail.com", "lang" => "ua"];
   
    // Формуємо базу імен користувачів
    $usersName = array_column($users, 'name');
    // Визначаємо унікальні імена
    $countName = array_count_values($usersName);
    foreach($countName as $name => $count){
        echo ($count > 1) ? ($name . "'s name repeat " . $count . " times<br/>") : '';
    } unset($usersName); unset($countName);

    // Формуємо массиви за мовами користувачів
    $usersLang = array_column($users, 'lang');
    $usersLang = array_unique($usersLang);
    foreach ($users as $key => $user){
        $nameArray = $user['lang'];
        $$nameArray[$key] = $user;
    }
    //Виводимо на екран відсортовані масиви
    foreach ($usersLang as $usersLang) {
        echo "Користувачі з мовою " . $usersLang . " :<br/>";
        echo "<pre>";
        var_export($$usersLang);
        echo "<pre/>";
    }
    //Сформуємо зворотній масив користувачів
    $usersReverse = array_reverse($users, TRUE);

    echo "Массив користувачів у зворотньму порядку:<br/>";
    echo "<pre>";
    var_export($usersReverse);
    echo "<pre/>";
?>    