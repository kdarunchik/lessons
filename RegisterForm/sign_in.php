<?php
require_once("config.php");

if (isset($_POST) && !empty($_POST)){  
  $login = $_POST['name'];
  $password = $_POST['password'];
  if(file_exists("loginFile.txt")){
    $equal = FALSE;
    $handle = fopen("loginFile.txt", "r");
    while ($userinfo = fscanf($handle, "%s %s")) {
      list ($name, $pas) = $userinfo;
      if ($login === $name && $password === $pas){
        $equal = TRUE;
        $hello = "Hello " . $login;
        $filename = $login.".txt";
        if (file_exists($filename)) {
          $val = file_get_contents($filename);
          file_put_contents($filename, $val+1);
        } else {
          file_put_contents($filename, "1");
        }
      }
    }
    fclose($handle);
    if ($equal == FALSE){
      $error = "Користувач не авторизований";
    }
  } else {
    $error = "Системний файл даних не знайдено";
  }
}
require_once(ROOT_PATH."/views/form_sign_in.php");
?>