<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Login</title>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600' rel='stylesheet' type='text/css'>
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo SITE_URL."/style.css";?>"
</head>
<body>
  <div class="testboxLog">
  <h1><?php echo (!empty($hello)) ? $hello : "Welcome";?></h1>
  <form class="form" action="<?php echo SITE_URL."/sign_in.php";?>" method="POST">
    <hr>
    <label id="icon" for="name"><i class="icon-user"></i></label>
    <input type="text" name="name" id="name" placeholder="Username" value="" required>
    <label id="icon" for="name"><i class="icon-shield"></i></label>
    <input type="password" name="password" id="name" placeholder="Password" value="" required>
    <hr>
    <div class="buttonHolder"><input type="submit" value="Login"></div>
    <?php echo (!empty($error)) ? "<hr><div> $error </div>" : "";?> 
    </form>
    </div>
</body>
</html>