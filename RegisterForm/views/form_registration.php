<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Register Form</title>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600' rel='stylesheet' type='text/css'>
  <link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo SITE_URL."/style.css";?>"/>
</head>
<body>
  <div class="testboxReg">
  <h1>Register Account Form</h1>
  <form class="form" action="<?php echo SITE_URL."/registration.php";?>" method="POST">
	<hr>
	
	<label id="icon" for="name"><i class="icon-user"></i></label>
  <input type="text" name="name" id="name" placeholder="UserName" value="" required>
	
	<label id="icon" for="name"><i class="icon-envelope "></i></label>
	<input type="text" name="email" id="name" placeholder="Email" value="" required>
	
	<label id="icon" for="name"><i class="icon-shield"></i></label>
 	<input type="password" name="password" id="name" placeholder="Password" value="" required>
	
	<div class="gender">
    <input type="radio" name="gender" value="Male" > 
    <label for="male" class="radio">Male</label>
    <input type="radio" name="gender" value="Female" >
    <label for="female" class="radio">Female</label>
    </div>
    
    <label id="icon" for="name"><i class="icon-globe"></i></label>
    <select name="language" required>
    <option value="" disabled selected>Select language</option>
    <?php $all_language = array("en" => "English", "ua" => "Ukrainian", "ru" => "Russian");
    foreach ($all_language as $k => $v) {
    echo "<option value=".$k.">".$v."</option>";} ?>
    </select>
    <hr>
    <hr>
    <div class="buttonHolder"><input type="submit" value="Register"></div>
    <?php echo (!empty($error)) ? "<hr><div> $error </div>" : "";?> 
    </form>
    </div>

	Your language <?php if (!empty($_lang)){var_export($_lang);} else echo "pusto";?>
	
</body>
</html>
